		<div class="indexblock" id="mathindexblock">
			<h1>{{.Mathh}}</h1>
			<p>{{.Mathp}}</p>
			<div class="button" onclick="MathOpen();"><h6>{{.Readmore}}</h6></div>
			<img src="/static/images/math.png" id="mathfrontimg">
		</div>
		<div class="mainblock" id="mathmainblock">
		</div>
		<div class="footer" id="mathfooter"><a href="#">&copy; MrPark 2015</a></div>
		<div class="indexblock" id="physindexblock">
			<h1>{{.Physh}}</h1>
			<p>{{.Physp}}</p>
			<div class="button" onclick="PhysicsOpen();"><h6>{{.Readmore}}</h6></div>
			<img src="/static/images/physics.png" id="physfrontimg">
		</div>
		<div class="mainblock" id="physmainblock">
		</div>
		<div class="footer" id="physfooter"><a href="#">&copy; MrPark 2015</a></div>
		<div class="indexblock" id="problemsindexblock">
			<h1>{{.Problemsh}}</h1>
			<p>{{.Problemsp}}</p>
			<div class="button" onclick="ProblemsOpen();"><h6>{{.Readmore}}</h6></div>
			<img src="/static/images/problems.png" id="problemsfrontimg">
		</div>
		<div class="mainblock" id="problemsmainblock">
		</div>
		<div class="footer" id="problemsfooter"><a href="#">&copy; MrPark 2015</a></div>
		<div class="indexblock" id="scientistindexblock">
			<h1>Einstein</h1>
			<p><i>"As far as the laws of mathematics refer to reality, they are not certain, and as far as they are certain, they do not refer to reality."</i></p>
			<div class="button" onclick="ScientistOpen();"><h6>{{.Readmore}}</h6></div>
			<img src="/static/scientists/einstein.jpg" id="scientistfrontimg">
		</div>
		<div class="mainblock" id="scientistmainblock">
		</div>
		<div class="footer" id="scientistfooter"><a href="#">&copy; MrPark 2015</a></div>